﻿@echo off
echo NOTA: este .bat hay que ejecutarlo dentro del directorio en 
echo el que se desea actuar.
echo.
echo.
echo.
echo NOTA: Los archivos temporales de LaTeX
echo irán a la carpeta .texpadtmp
echo.
echo.
echo Ejecutamos el traslado de archivos.
set extension1=.acn
set extension2=.aux
set extension3=.bcf
set extension4=.glo
set extension5=.ist
set extension6=.log
set extension7=.out
set extension8=.xml
set extension9=.soc
set extension10=.toc
set extension11=.xwm
set extension12=.gz 
echo.
echo. 
set destino=.texpadtmp
mkdir "%destino%"

for /R %%x in (*%extension1%) do move "%%x" "%destino%"
for /R %%a in (*%extension2%) do move "%%a" "%destino%"
for /R %%b in (*%extension3%) do move "%%b" "%destino%"
for /R %%c in (*%extension4%) do move "%%c" "%destino%"
for /R %%d in (*%extension5%) do move "%%d" "%destino%"
for /R %%e in (*%extension6%) do move "%%e" "%destino%"
for /R %%f in (*%extension7%) do move "%%f" "%destino%"
for /R %%g in (*%extension8%) do move "%%g" "%destino%"
for /R %%h in (*%extension9%) do move "%%h" "%destino%"
for /R %%i in (*%extension10%) do move "%%i" "%destino%"
for /R %%j in (*%extension11%) do move "%%j" "%destino%"
for /R %%k in (*%extension12%) do move "%%k" "%destino%"
echo Traslado finalizado
echo .

EXIT